# MaykiCatalog  

Проект сгенерирован с использованием [Angular CLI](https://github.com/angular/angular-cli) версии 1.4.2.  

## Запуск приложения на dev-сервере  
1. Выполнить npm install  
2. Запустить dev-сервер:  
2.1. Windows: .\node_modules\.bin\ng.cmd serve  
2.2. Unix: ./node_modules/.bin/ng serve  
2.3. Если Angular CLI установлен глобально, то ng serve  
3. Открыть в браузере `http://localhost:4200/`
