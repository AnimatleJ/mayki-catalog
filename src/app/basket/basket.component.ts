import {Component, OnInit} from '@angular/core';
import {BasketItem} from '../shared/basket-item';
import {BasketService} from '../shared/basket.service';

@Component({
    selector: 'app-basket',
    templateUrl: './basket.component.html',
    styles: []
})
export class BasketComponent implements OnInit {
    public basketItems: BasketItem[] = [];

    constructor(private basketService: BasketService) {
    }

    ngOnInit() {
        this.basketItems = this.basketService.getItems();
    }

    onChangeItem() {
        // значение уже обновлено, т.к. объект basketItems является ссылкой на BasketService.data
        // осталось только сохранить корзину в локальном хранилище
        this.basketService.storeData();
    }

    get summ() {
        let summ = 0;
        for (let item of this.basketItems) {
            summ += item.count * item.item.price;
        }
        return summ;
    }

    deleteItem(item: BasketItem) {
        if (!confirm('Вы действительно хотите удалить "' + item.item.title + '" из корзины?')) return;
        this.basketService.deleteItem(item.item);
    }
}
