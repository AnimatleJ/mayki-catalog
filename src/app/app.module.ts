import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CatalogComponent} from './catalog/catalog.component';
import {BasketComponent} from './basket/basket.component';
import {CatalogItemService} from './shared/catalog-item.service';
import {CatalogListComponent} from './catalog/catalog-list/catalog-list.component';
import {HttpModule} from '@angular/http';
import {SharedModule} from './shared/shared.module';
import {BasketService} from './shared/basket.service';
import {LocalStorageService} from './shared/local-storage.service';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        CatalogComponent,
        BasketComponent,
        CatalogListComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpModule,
        FormsModule,
        SharedModule
    ],
    providers: [CatalogItemService, LocalStorageService, BasketService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
