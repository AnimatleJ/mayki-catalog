import {Component, Input, OnInit} from '@angular/core';
import {CatalogItem} from '../../shared/catalog-item';
import {BasketService} from '../../shared/basket.service';

@Component({
    selector: 'app-catalog-list',
    templateUrl: './catalog-list.component.html',
    styleUrls: ['./catalog-list.component.css'],
})
export class CatalogListComponent implements OnInit {
    @Input() items: CatalogItem[] = [];

    constructor(private basketService: BasketService) {
    }

    ngOnInit() {
    }

    itemInBasket(item: CatalogItem): boolean {
        let index = this.basketService.findItemIndex(item);
        return index !== -1;
    }

    addToBasket(item: CatalogItem) {
        this.basketService.add(item);
    }
}
