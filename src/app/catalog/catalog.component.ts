import {Component, OnInit} from '@angular/core';
import {CatalogItemService} from '../shared/catalog-item.service';
import {CatalogItem} from '../shared/catalog-item';
import {CatalogItemResponse} from '../shared/catalog-item-response';

@Component({
    selector: 'app-catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.css'],
})
export class CatalogComponent implements OnInit {
    public items: CatalogItem[] = [];
    public totalCount: number = 0;
    public isLoading = false;
    private pagingRequest: any = {
        limit: 12,
        offset: 0
    };

    constructor(private catalogItemService: CatalogItemService) {
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.isLoading = true;
        this.catalogItemService.getList(this.pagingRequest).subscribe(
            (res: CatalogItemResponse) => {
                this.isLoading = false;
                this.items = res.items;
                this.totalCount = res.totalCount;
            },
            error => {
                this.isLoading = false;
            }
        )
    }

    setPagingRequest(pagingRequest: any) {
        this.pagingRequest = pagingRequest;
        this.load();
    }

}
