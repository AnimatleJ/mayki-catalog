export class CatalogItem {
    id: string;
    title: string;
    price: number;
    old_price?: number = null;
    img?: string = null;
    constructor(id: string, title: string, price: number, old_price?: number, img?: string) {
        this.id = id;
        this.title = title;
        this.price = price;
        if(old_price) {
            this.old_price = old_price;
        }
        if(img) {
            this.img = img;
        }
    }
}
