import {Injectable} from '@angular/core';
import {CatalogItem} from './catalog-item';
import {BasketItem} from './basket-item';
import {LocalStorageService} from './local-storage.service';

@Injectable()
export class BasketService {
    private dataKey = 'BasketService.data';
    private data: BasketItem[] = [];

    constructor(private localStorageService: LocalStorageService) {
        this.loadData();
    }

    add(item: CatalogItem) {
        let index = this.findItemIndex(item);
        if(index !== -1) return;
        this.data.push(new BasketItem(item, 1));
        this.storeData();
    }

    deleteItem(item: CatalogItem) {
        let index = this.findItemIndex(item);
        if(index === -1) return;

        this.data.splice(index, 1);
        this.storeData();
    }

    getItems(): BasketItem[] {
        return this.data;
    }

    private loadData() {
        let data = this.localStorageService.getValue(this.dataKey);
        if(typeof data !== 'object' || data === null) return;

        for(let row of data) {
            let item = new CatalogItem(row.item.id, row.item.title, row.item.price, row.item.old_price, row.item.img);
            let basketItem = new BasketItem(item, row.count);
            this.data.push(basketItem);
        }
    }

    findItemIndex(item: CatalogItem) {
        let index = this.data.findIndex(element => {
            return item.id == element.item.id;
        });
        return index;
    }

    storeData() {
        this.localStorageService.setValue(this.dataKey, this.data);
    }
}
