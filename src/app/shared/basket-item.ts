import {CatalogItem} from './catalog-item';
export class BasketItem {
    item: CatalogItem;
    count: number;
    constructor(item: CatalogItem, count: number) {
        this.item = item;
        this.count = count;
    }
}
