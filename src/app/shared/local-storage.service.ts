import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

    constructor() {
    }

    getValue(key: string): any {
        let value = window.localStorage.getItem(key);
        if(typeof value !== 'string') return value;
        return JSON.parse(value);
    }

    setValue(key: string, data: any) {
        window.localStorage.setItem(key, JSON.stringify(data));
    }
}
