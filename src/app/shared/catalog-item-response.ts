import {CatalogItem} from './catalog-item';
export class CatalogItemResponse {
    items: CatalogItem[] = [];
    totalCount: number = 0;
}
