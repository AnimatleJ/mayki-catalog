import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {environment} from '../../environments/environment';
import {CatalogItem} from './catalog-item';
import {CatalogItemResponse} from './catalog-item-response';

@Injectable()
export class CatalogItemService {
    private url:string;

    constructor(private http: Http) {
        this.url = environment.VM_API_URL + '/items';
    }

    /*
     * Получение списка товаров
     * */
    getList(paging?: any): Observable<CatalogItemResponse> {
        let params: any = {
            'access-token': environment.VM_API_TOKEN
        };
        if (paging) {
            params.limit = paging.limit;
            params.offset = paging.offset;
        }
        return this.http.get(this.url, {params: params})
            .map((res: Response) => {
                let data = res.json();
                let result = new CatalogItemResponse();
                for (let rawItem of data.items) {
                    let itemImage = null;
                    if (typeof rawItem.sides === 'object' && rawItem.sides.length > 0) {
                        itemImage = rawItem.sides[0]['small'] || null;
                    }
                    //rawItem.old_price = 45785;
                    let item = new CatalogItem(rawItem.id, rawItem.title, +rawItem.price, rawItem.old_price, itemImage);
                    result.items.push(item);
                }
                result.totalCount = +data.total_items;
                return result;
            })
            .catch(error => {
                return Observable.throw(error);
            })
    }

}
