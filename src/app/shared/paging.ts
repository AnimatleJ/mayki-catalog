export class Paging {
    countPerPage: number;
    totalCount: number;
    page: number;

    constructor(page: number, countPerPage: number, totalCount: number, ) {
        this.page = page;
        this.totalCount = totalCount;
        this.countPerPage = countPerPage;
    }

    getRequest() {
        return {
            limit: this.countPerPage,
            offset: this.countPerPage*(this.page-1)
        }
    }
}
